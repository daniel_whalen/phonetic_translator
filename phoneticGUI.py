#!/usr/bin/env python3
# A simple translator of letter strings into the NATO phonetic alphabet
# Now with a GUI, for those who can't handle plain text
# Version 0.9.3
# Last edit: 02/24/2016
# Written by: Daniel Whalen
# Contact information: i3eaker@yahoo.com

# Import the GUI toolkit module
import tkinter

# Import the sys module for proper exiting
import sys

# Put the application in a class
class phoneticGUI(tkinter.Tk):
    # Initial configuration
    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()
    # Create the GUI
    def initialize(self):
        self.grid()
        # Text entry box for the word to be translated
        self.entry_translate = tkinter.StringVar()
        self.entry =tkinter.Entry(self,
                textvariable=self.entry_translate,fg=fgnd,bg=bgnd)
        self.entry.grid(column=0,row=0,columnspan=7,sticky='EW')
        self.entry.bind("<Return>", self.OnButton0Click)
        self.entry_translate.set(u"Word or phrase to be translated-")
        # Button0 (Translate!)
        button0 = tkinter.Button(self,text=u"Translate!",
                command=self.OnButton0Click,fg=fgnd,bg=bgnd)
        button0.grid(column=7,row=0,columnspan=2)
        # Button1 (Exit.)
        button1 = tkinter.Button(self,text=u"Exit.",
                command=self.OnButton1Click,fg=fgnd,bg=bgnd)
        button1.grid(column=9,row=0)
        # Area for translated text to be displayed
        # Wraps text at 496 pixels
        self.label_translation = tkinter.StringVar()
        label = tkinter.Label(self,textvariable=self.label_translation,
                anchor="center",wraplength=496,height=10,fg=fgnd,bg=bgnd)
        label.grid(column=0,row=1,columnspan=10,rowspan=10,sticky='EW')
        self.label_translation.set(u"Translation will be displayed here.")
        # Grid configuration (10 total columns, 500 pixels wide)
        self.grid_columnconfigure(0,weight=1,minsize=50)
        self.grid_columnconfigure(1,weight=1,minsize=50)
        self.grid_columnconfigure(2,weight=1,minsize=50)
        self.grid_columnconfigure(3,weight=1,minsize=50)
        self.grid_columnconfigure(4,weight=1,minsize=50)
        self.grid_columnconfigure(5,weight=1,minsize=50)
        self.grid_columnconfigure(6,weight=1,minsize=50)
        self.grid_columnconfigure(7,weight=1,minsize=50)
        self.grid_columnconfigure(8,weight=1,minsize=50)
        self.grid_columnconfigure(9,weight=1,minsize=50)
        # Background color
        self.configure(bg=bgnd_main)
        # Resizability, only vertically
        self.resizable(False,True)
        # Window size fixed, not dependent on text entered
        self.update()
        self.geometry(self.geometry())
        # Set the focus on the text entry box
        self.entry.focus_set()
        self.entry.selection_range(0, tkinter.END)

    # Button0 function (run the application)
    def OnButton0Click(self, *args):
        # Separate the entered text into a usable list
        def translate_to_list(translate):
            length_str = len(translate)
            a = 0
            while a != length_str:
                translate_list.append(translate[a])
                a += 1
            return translate_list

        # Dictionary of phonetic translations
        phonetic_dict = {
                "a": "Alpha",
                "b": "Bravo",
                "c": "Charlie",
                "d": "Delta",
                "e": "Echo",
                "f": "Foxtrot",
                "g": "Golf",
                "h": "Hotel",
                "i": "India",
                "j": "Juliet",
                "k": "Kilo",
                "l": "Lima",
                "m": "Mike",
                "n": "November",
                "o": "Oscar",
                "p": "Papa",
                "q": "Quebec",
                "r": "Romeo",
                "s": "Sierra",
                "t": "Tango",
                "u": "Uniform",
                "v": "Victor",
                "w": "Whiskey",
                "x": "X-ray",
                "y": "Yankee",
                "z": "Zulu",
#                " ": "SPACE",
                " ": "\n",
                ".": "DOT",
                "-": "DASH",
        }

        # Create a new list of the phonetic keys for each character
        # in translate_list
        def list_to_phonetic(translate_list):
            for letter in translate_list:
                if letter in phonetic_dict:
                    phonetic_list.append(phonetic_dict[letter])
            return phonetic_list

        # Display the translation to the user
        def display_to_user(phonetic_list):
            what_to_print = ""
            newline_check = "No"
            for phonetic_letter in phonetic_list:
                if what_to_print == "":
                    what_to_print = what_to_print + phonetic_letter
                else:
                    if newline_check == "No":
                        what_to_print = what_to_print + ", " + phonetic_letter
                        newline_check = "No"
                        if phonetic_letter == "\n":
                            newline_check = "Yes"
                    else:
                        what_to_print = what_to_print + phonetic_letter
                        newline_check = "No"
            self.label_translation.set( what_to_print )

        # Main function calls
        translate = self.entry_translate.get().lower()
        translate_list = []
        translate_to_list(translate)
        phonetic_list = []
        list_to_phonetic(translate_list)
        display_to_user(phonetic_list)

    # Button1 function (exit the application)
    def OnButton1Click(self):
        sys.exit()

# Application loop
if __name__ == "__main__":
    # Variables for formatting
    bgnd = "dark olive green"
    bgnd_main = "dark olive green"
    fgnd = "gainsboro"

    # Run the application
    app = phoneticGUI(None)
    app.title('NATO phonetic alphabet translator.')
    app.mainloop()
