#!/usr/bin/env python
# A simple translator of letter strings into the NATO phonetic alphabet
# Written by: Daniel Whalen
# Contact information: i3eaker@yahoo.com

# Begin the phonetic alphabet translator
print("Welcome to the NATO phonetic alphabet Translator!")

# Seperate the entered text into a usable list
def translate_to_list(translate):
    length_str = len(translate)
    a = 0
    while a != length_str:
        translate_list.append(translate[a])
        a += 1
    return translate_list

# Dictionary of phonetic translations
phonetic_dict = {
        "a": "Alpha",
        "b": "Bravo",
        "c": "Charlie",
        "d": "Delta",
        "e": "Echo",
        "f": "Foxtrot",
        "g": "Golf",
        "h": "Hotel",
        "i": "India",
        "j": "Juliet",
        "k": "Kilo",
        "l": "Lima",
        "m": "Mike",
        "n": "November",
        "o": "Oscar",
        "p": "Papa",
        "q": "Quebec",
        "r": "Romeo",
        "s": "Sierra",
        "t": "Tango",
        "u": "Uniform",
        "v": "Victor",
        "w": "Whiskey",
        "x": "X-ray",
        "y": "Yankee",
        "z": "Zulu",
        " ": "SPACE",
        ".": "DOT",
        "-": "DASH",
}

# Create a new list of the phonetic keys for each character in translate_list
def list_to_phonetic(translate_list):
    for letter in translate_list:
        if letter in phonetic_dict:
            phonetic_list.append(phonetic_dict[letter])
    return phonetic_list

# Display the translation to the user
def display_to_user(phonetic_list):
    what_to_print = ""
    for phonetic_letter in phonetic_list:
        if what_to_print == "":
            what_to_print = what_to_print + phonetic_letter
        else:
            what_to_print = what_to_print + ", " + phonetic_letter
    print(what_to_print)

# Print blank lines for readability
def spaces():
    print("|")
    print("|")
    print("|")
    print("|")
    print("-----------------")
    return

# Main loop, keeping the application running until stopped by the user
while 1 == 1:
    translate_prompt = "Word or phrase to be translated- "
    translate = input(translate_prompt).lower()
    translate_list = []
    translate_to_list(translate)
    phonetic_list = []
    list_to_phonetic(translate_list)
    display_to_user(phonetic_list)
    spaces()
